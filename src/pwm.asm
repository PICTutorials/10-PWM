;   PWM - Play with the PWM of the PIC turning a LED
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of PWM
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;PWM (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the PWM frequency
; - RB1 - 7 - bit 1 of the PWM frequency
; - RB2 - 8 - bit 2 of the PWM frequency
; - RA1 - 18 - Input switch for frequency change
; - RB3 - 9 - LED to indicate if the PWM is working
;=============================

;Variables
;==============================
COUNTER EQU 0x27
TEMP EQU 0x28
BTNPRESSED EQU 0x29

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start		
	
;Functions
;==============================
	include digitecnology.inc
ChangeFrequency
	call chgbnk0
	incf COUNTER,1
	movf COUNTER,0
	sublw .8
	btfsc STATUS,2
	clrf COUNTER
	return
ConfigurePWM
	call chgbnk0
	;Deactivate PWM
	clrf CCP1CON
	;Set a PWM of 244 Hz
	bsf T2CON,1 ;Set TIMER2 prescale value (1:16)
	movlw 0xFF ;Set Period register to 255
	call chgbnk1
	movwf PR2
	call chgbnk0
	movf COUNTER,0
        sublw .7
        btfss STATUS,2 ; If COUNTER = 7
        goto $+6
        movlw b'11111111' ;Set a duty cycle of 100%
        movwf CCPR1L
        bsf CCP1CON,5
        bsf CCP1CON,4
        goto TurnOnPWM
	movf COUNTER,0
        sublw .6
        btfss STATUS,2 ; If COUNTER = 6
        goto $+6
        movlw b'10111111' ;Set a duty cycle of 75%
        movwf CCPR1L
        bsf CCP1CON,5
        bsf CCP1CON,4
        goto TurnOnPWM
	movf COUNTER,0
        sublw .5
        btfss STATUS,2 ; If COUNTER = 5
        goto $+6
        movlw b'10011111' ;Set a duty cycle of 62.5%
        movwf CCPR1L
        bsf CCP1CON,5
        bsf CCP1CON,4
        goto TurnOnPWM
	movf COUNTER,0
	sublw .4
	btfss STATUS,2 ; If COUNTER = 4
	goto $+6
	movlw b'01111111' ;Set a duty cycle of 50%
	movwf CCPR1L
	bsf CCP1CON,5
	bsf CCP1CON,4
	goto TurnOnPWM
	movf COUNTER,0
	sublw .3
	btfss STATUS,2 ; If COUNTER = 3
	goto $+6
	movlw b'01011111' ;Set a duty cycle of 37.5%
        movwf CCPR1L
        bsf CCP1CON,5
        bsf CCP1CON,4
	goto TurnOnPWM
	movf COUNTER,0
        sublw .2
        btfss STATUS,2 ; If COUNTER = 2
        goto $+6
        movlw b'00111111' ;Set a duty cycle of 25%
        movwf CCPR1L
        bsf CCP1CON,5
        bsf CCP1CON,4
	goto TurnOnPWM
	movf COUNTER,0
        sublw .1
        btfss STATUS,2 ; If COUNTER = 1
        goto $+6
        movlw b'00011111' ;Set a duty cycle of 12.5%
        movwf CCPR1L
        bsf CCP1CON,5
        bsf CCP1CON,4
	goto TurnOnPWM
	movf COUNTER,0
        sublw .0
        btfss STATUS,2 ; If COUNTER = 0
        goto $+5
        movlw b'00000000' ;Set a duty cycle of 0%
        movwf CCPR1L
        bcf CCP1CON,5
        bcf CCP1CON,4        
TurnOnPWM	
	bsf T2CON,2 ;Enable TIMER2
	bsf CCP1CON,3 ;Turn On PWM
	bsf CCP1CON,2
	return
ShowFrequency	
	call chgbnk0
	movf COUNTER,0
	andlw b'00000111'
	movwf TEMP
	movf PORTB,0
	andlw b'11111000'
	iorwf TEMP,0
	movwf PORTB
	return
Wait
	;Wait 10 ms
	movlw .246
	movwf DELAY1
	movlw .14
	movwf DELAY2
	call delay2
	return
;Program
;==============================
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	bcf TRISB,0 ;Output
	bcf TRISB,1 ;Output
	bcf TRISB,2 ;Output
	bcf TRISB,3 ;Output
	bsf TRISA,1 ;Input
	;Deactivate analog comparators
	call chgbnk0
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0	
	;Clear counter
	clrf COUNTER
	;Configure PWM
	call ConfigurePWM	
	call ShowFrequency
Cycle			
	;Check input
	btfsc PORTA,1
	goto $+0xB ;11
	call Wait
	btfsc PORTA,1
	goto $+8
	btfsc BTNPRESSED,0
	goto $+7
	call ChangeFrequency
	call ConfigurePWM
	call ShowFrequency
	bsf BTNPRESSED,0
	goto $+2
	bcf BTNPRESSED,0
	goto Cycle
	end
